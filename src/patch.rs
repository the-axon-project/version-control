// Patch
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use chrono::{DateTime, Utc};
use std::collections::BTreeSet;

pub struct Patch {
    pub header: PatchHeader,
    pub dependencies: BTreeSet<Hash>,
    pub changes: Vec<Change>,
}

pub struct PatchHeader {
    pub authors: Vec<String>,
    pub title: String,
    pub description: Option<String>,
    pub timestamp: DateTime<Utc>,
}

pub struct Change {
}

const SHA512_BYTES: usize = 512 / 8;

pub enum Hash {
    Sha3_512([u8; SHA512_BYTES]),
}

pub struct Branch {
    pub name: String,
}

pub struct Graph {
    pub bytes: Vec<u8>,
}
